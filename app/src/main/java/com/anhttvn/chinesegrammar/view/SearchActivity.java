package com.anhttvn.chinesegrammar.view;

import android.content.Intent;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.chinesegrammar.R;
import com.anhttvn.chinesegrammar.base.BaseActivity;
import com.anhttvn.chinesegrammar.base.adapter.GrammarAdapter;
import com.anhttvn.chinesegrammar.base.model.News;
import com.anhttvn.chinesegrammar.util.Config;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseActivity  implements GrammarAdapter.EventGrammar{
  private com.anhttvn.chinesegrammar.databinding.ActivitySearchBinding searchBinding;
  private List<News> grammars = new ArrayList<>();
  private GrammarAdapter grammarAdapter;
  private int countSearch = 0;
  @Override
  public void init() {
    searchBinding.search.setVisibility(View.GONE);
    grammars = db.allGrammars();
    adapter(grammars);

    searchBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return false;
      }
      @Override
      public boolean onQueryTextChange(String newText) {
        grammarAdapter.getFilter().filter(newText);
        return false;
      }
    });
  }

  @Override
  public View contentView() {
    searchBinding = com.anhttvn.chinesegrammar.databinding.ActivitySearchBinding.inflate(getLayoutInflater());
    return searchBinding.getRoot();
  }

  private void adapter(List<News> grammars) {
    if (grammars != null && grammars.size() > 0) {
      searchBinding.grammars.setVisibility(View.VISIBLE);
      grammarAdapter = new GrammarAdapter(this, grammars, this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
      searchBinding.grammars.setLayoutManager(layoutManager);
      searchBinding.grammars.setItemAnimator(new DefaultItemAnimator());
      searchBinding.grammars.setAdapter(grammarAdapter);
      grammarAdapter.notifyDataSetChanged();
    } else {
      searchBinding.grammars.setVisibility(View.GONE);
    }

  }

  @Override
  public void onClickDetail(int position) {
    Intent intent = new Intent(this, GrammarDetail.class);
    intent.putExtra(Config.KEY_DETAIL, grammars.get(position));
    startActivity(intent);
  }

  public void onBack(View view) {
    finish();
  }

  public void onSearch(View view) {
    countSearch += 1;
    if (countSearch % 2 == 0) {
      searchBinding.search.setVisibility(View.GONE);
      searchBinding.imgSearch.setImageResource(R.drawable.ic_search);
    } else {
      searchBinding.search.setVisibility(View.VISIBLE);
      searchBinding.search.onActionViewExpanded();
      searchBinding.imgSearch.setImageResource(R.drawable.ic_search_open);
    }
  }
}
