package com.anhttvn.chinesegrammar;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import com.anhttvn.chinesegrammar.databinding.ActivityMainBinding;
import com.anhttvn.chinesegrammar.util.Config;
import com.anhttvn.chinesegrammar.view.PrivatePolice;
import com.anhttvn.chinesegrammar.view.SearchActivity;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends com.anhttvn.chinesegrammar.base.BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

  private ActivityMainBinding mainBinding;


  @Override
  public void init() {
    setSupportActionBar(mainBinding.header.toolbar);
    mainBinding.navView.setNavigationItemSelectedListener(this);

    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainBinding.drawerLayout, mainBinding.header.toolbar,
            R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    mainBinding.drawerLayout.addDrawerListener(toggle);
    toggle.syncState();
    mainBinding.header.toolbar.setTitle(getString(R.string.home));
    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
              new com.anhttvn.chinesegrammar.fragment.HomeFragment()).commit();
      mainBinding.navView.setCheckedItem(R.id.home);

    }

  }

  @Override
  public View contentView() {
    mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
    return mainBinding.getRoot();
  }



  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.home:
        mainBinding.header.toolbar.setTitle(getString(R.string.home));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new com.anhttvn.chinesegrammar.fragment.HomeFragment()).commit();
        break;
      case R.id.grammarBasic:
        mainBinding.header.toolbar.setTitle(getString(R.string.grammarBasic));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new com.anhttvn.chinesegrammar.fragment.GrammarFragment(Config.GRAMMAR_BASIC)).commit();
        break;
      case R.id.grammarAdvanced:
        mainBinding.header.toolbar.setTitle(getString(R.string.grammarAdvanced));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new com.anhttvn.chinesegrammar.fragment.GrammarFragment(Config.GRAMMAR_ADVANCED)).commit();
        break;
      case R.id.tips:
        mainBinding.header.toolbar.setTitle(getString(R.string.tipsEnglish));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new com.anhttvn.chinesegrammar.fragment.GrammarFragment(Config.LABEL_TIPS_TRICKS)).commit();
        break;
      case R.id.favorite:
        mainBinding.header.toolbar.setTitle(getString(R.string.favorite));
        getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                new com.anhttvn.chinesegrammar.fragment.FavoriteFragment()).commit();
        break;
      case R.id.search:
        Intent intent1 = new Intent(this, SearchActivity.class);
        startActivity(intent1);
        break;

      case R.id.share:
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app));
        i.putExtra(Intent.EXTRA_TEXT, Config.URL_APP);
        startActivity(Intent.createChooser(i, "Share"));
        break;
      case R.id.rate:
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(com.anhttvn.chinesegrammar.util.Config.URL_APP));
        startActivity(browserIntent);
        break;
      case R.id.privacyPolice:
        Intent intent = new Intent(this, PrivatePolice.class);
        startActivity(intent);
        break;
      default:
        break;
    }
    mainBinding.drawerLayout.closeDrawer(GravityCompat.START);
    return true;
  }


  @Override
  public void onBackPressed() {
    showExit();
  }

  private void showExit() {
      AlertDialog.Builder dialogBuilder =	new AlertDialog.Builder(this);
      LayoutInflater inflater	= this.getLayoutInflater();
      View dialogView	= inflater.inflate(R.layout.layout_exits, null);
      isBannerADS(dialogView.findViewById(R.id.ads));
      dialogBuilder.setView(dialogView);
      AlertDialog b = dialogBuilder.create();
      dialogView.findViewById(R.id.btnNo).setOnClickListener(v -> {
        b.dismiss();
      });
      dialogView.findViewById(R.id.btnYes).setOnClickListener(v -> {
        finish();
      });
      b.show();
  }
  @Override
  protected void onDestroy() {
    super.onDestroy();

  }
}