package com.anhttvn.chinesegrammar.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Config {

  /**
   * config send data
   */
  public static final String KEY_GRAMMAR = "1010101010";
  public static final String KEY_DETAIL = "2020202020";

  public static final String GRAMMAR_ADVANCED ="ngu-phap-nang-cao";
  public static final String GRAMMAR_BASIC ="ngu-phap-co-ban";
  public static final String LABEL_TOP_NEW = "new";
  public static final String LABEL_TIPS_TRICKS = "tips-trick-english";
  public static final String LABEL_GRAMMAR ="LABEL_GRAMMAR";

  public static final String URL_APP = "https://play.google.com/store/apps/details?id=com.anhttvn.chinesegrammar";
  public  static final int PERMISSION_REQUEST_CODE = 7;
  public static final String KEY = "AIzaSyDNHWlzQeAWofuuEdV0jN9suuorGTLHT-Y";



  public static String findUrlImage(String content) {
    if (content == null || content.isEmpty()) {
      return null;
    }
    String regex = "https?:/(?:/[^/]+)+\\.(?:jpg|gif|png|JPG)";
    Pattern pat = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pat.matcher(content);
    String url = null;
    while (matcher.find()) {
      if (content.substring(matcher.start(0),matcher.end(0)) != null ||
              content.substring(matcher.start(0),matcher.end(0)).length() > 0) {
        url = content.substring(matcher.start(0),matcher.end(0));
        break;
      }
    }
    return  url;
  }

  public static String formatTitle(String title) {
    if (title == null || title.length() < 1 || title.isEmpty()) {
      return "Ngữ Pháp Tiếng Trung";
    }
    switch (title) {
      case LABEL_TOP_NEW:
        return "Tin Tức";
      case LABEL_TIPS_TRICKS:
        return "Mẹo Học Tiếng Trung";
      case GRAMMAR_BASIC:
        return "Ngữ pháp cơ bản";
      case GRAMMAR_ADVANCED:
        return "Ngữ pháp nâng cao";
      default: return "Ngữ Pháp Tiếng Trung";
    }

  }
}

