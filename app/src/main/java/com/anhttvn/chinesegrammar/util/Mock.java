package com.anhttvn.chinesegrammar.util;

import com.anhttvn.chinesegrammar.base.model.Label;

import java.util.ArrayList;
import java.util.List;

public class Mock {

  public static List<Label> labels = new ArrayList<>();

  public static void addLabels() {
    labels = new ArrayList<>();
    Label label = new Label();
    label.setTitle("Ngu phap");
    label.setLabel("ung-dung-android");
    label.setImageURL("https://blogger.googleusercontent.com/img/a/AVvXsEgvrix_j5fRBpVXx0FHJvdgxP6_B7pkr_XtE8-hRLqZfZPl4BDaN2ppkV39aqXBZbiTaG-2uAh_-jcTuHJrrbYheyHyvQJjkbnb9Ua9zhZf9YSb-iOSm678NFKO_I0oQNUpuca_VHjJOh9Sir02aTGHUbhB8QvtP3JpeHocZc32EzSsOjP8cOf2xkGZ-w=s320");
    labels.add(label);

    label = new Label();
    label.setTitle("Ngu phap");
    label.setLabel("ung-dung-android");
    label.setImageURL("https://blogger.googleusercontent.com/img/a/AVvXsEgvrix_j5fRBpVXx0FHJvdgxP6_B7pkr_XtE8-hRLqZfZPl4BDaN2ppkV39aqXBZbiTaG-2uAh_-jcTuHJrrbYheyHyvQJjkbnb9Ua9zhZf9YSb-iOSm678NFKO_I0oQNUpuca_VHjJOh9Sir02aTGHUbhB8QvtP3JpeHocZc32EzSsOjP8cOf2xkGZ-w=s320");
    labels.add(label);

    label = new Label();
    label.setTitle("Ngu phap");
    label.setLabel("ung-dung-android");
    label.setImageURL("https://blogger.googleusercontent.com/img/a/AVvXsEgvrix_j5fRBpVXx0FHJvdgxP6_B7pkr_XtE8-hRLqZfZPl4BDaN2ppkV39aqXBZbiTaG-2uAh_-jcTuHJrrbYheyHyvQJjkbnb9Ua9zhZf9YSb-iOSm678NFKO_I0oQNUpuca_VHjJOh9Sir02aTGHUbhB8QvtP3JpeHocZc32EzSsOjP8cOf2xkGZ-w=s320");
    labels.add(label);

    label = new Label();
    label.setTitle("Ngu phap");
    label.setLabel("Angular");
    label.setImageURL("https://blogger.googleusercontent.com/img/a/AVvXsEgvrix_j5fRBpVXx0FHJvdgxP6_B7pkr_XtE8-hRLqZfZPl4BDaN2ppkV39aqXBZbiTaG-2uAh_-jcTuHJrrbYheyHyvQJjkbnb9Ua9zhZf9YSb-iOSm678NFKO_I0oQNUpuca_VHjJOh9Sir02aTGHUbhB8QvtP3JpeHocZc32EzSsOjP8cOf2xkGZ-w=s320");
    labels.add(label);

    label = new Label();
    label.setTitle("Ngu phap");
    label.setLabel("ung-dung-android");
    label.setImageURL("https://blogger.googleusercontent.com/img/a/AVvXsEgvrix_j5fRBpVXx0FHJvdgxP6_B7pkr_XtE8-hRLqZfZPl4BDaN2ppkV39aqXBZbiTaG-2uAh_-jcTuHJrrbYheyHyvQJjkbnb9Ua9zhZf9YSb-iOSm678NFKO_I0oQNUpuca_VHjJOh9Sir02aTGHUbhB8QvtP3JpeHocZc32EzSsOjP8cOf2xkGZ-w=s320");
    labels.add(label);
  }
}
