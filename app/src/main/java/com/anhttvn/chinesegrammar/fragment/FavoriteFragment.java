package com.anhttvn.chinesegrammar.fragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.chinesegrammar.base.BaseFragment;
import com.anhttvn.chinesegrammar.base.adapter.GrammarAdapter;
import com.anhttvn.chinesegrammar.base.model.News;
import com.anhttvn.chinesegrammar.databinding.FragmentFavoriteBinding;
import com.anhttvn.chinesegrammar.util.Config;
import com.anhttvn.chinesegrammar.view.GrammarDetail;

import java.util.ArrayList;
import java.util.List;

public class FavoriteFragment extends BaseFragment implements GrammarAdapter.EventGrammar {
  private GrammarAdapter grammarAdapter;
  private FragmentFavoriteBinding favoriteBinding;

  private List<News> grammars = new ArrayList<>();
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    favoriteBinding = FragmentFavoriteBinding.inflate(getLayoutInflater());
    return favoriteBinding.getRoot();
  }

  @Override
  protected void init() {

  }

  private void adapter(List<News> grammars) {
    if (grammars != null && grammars.size() > 0) {
      favoriteBinding.grammars.setVisibility(View.VISIBLE);
      favoriteBinding.noData.getRoot().setVisibility(View.GONE);
      grammarAdapter = new GrammarAdapter(getActivity(), grammars, this);
      RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
      favoriteBinding.grammars.setLayoutManager(layoutManager);
      favoriteBinding.grammars.setItemAnimator(new DefaultItemAnimator());
      favoriteBinding.grammars.setAdapter(grammarAdapter);
      grammarAdapter.notifyDataSetChanged();
    } else {
      favoriteBinding.grammars.setVisibility(View.GONE);
      favoriteBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }

  }

  @Override
  public void onClickDetail(int position) {
    Intent intent = new Intent(getActivity(), GrammarDetail.class);
    intent.putExtra(Config.KEY_DETAIL, grammars.get(position));
    startActivity(intent);
  }

  @Override
  public void onResume() {
    super.onResume();
    grammars = db.likeGrammars();
    adapter(grammars);
  }
}
