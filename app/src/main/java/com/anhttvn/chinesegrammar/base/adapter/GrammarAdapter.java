package com.anhttvn.chinesegrammar.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.chinesegrammar.R;
import com.anhttvn.chinesegrammar.base.model.News;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GrammarAdapter extends RecyclerView.Adapter<GrammarAdapter.GrammarViewHolder> implements View.OnClickListener, Filterable {

  private Context mContext;
  private List<News> mItems;
  private List<News> exampleListFull;
  private EventGrammar eventGrammar;


  public GrammarAdapter(Context context, List<News> items, EventGrammar clickDetail) {
    mContext = context;
    mItems = items;
    exampleListFull = new ArrayList<>(items);
    eventGrammar = clickDetail;
  }
  @NonNull
  @Override
  public GrammarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_adapter,parent,false);
    GrammarViewHolder itemView = new GrammarViewHolder(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull GrammarViewHolder holder, int position) {
    News items = mItems.get(position);
    if (items != null) {
      holder.title.setText(items.getTitle());
      if (items.getPublished() != null) {
        holder.date.setText(formatDate(items.getPublished()));
      }
      if (items.getAuthor() != null && items.getAuthor().getDisplayName() != null) {
        holder.author.setText(items.getAuthor().getDisplayName());
      }

      if (findUrlImage(items.getContent()) == null || findUrlImage(items.getContent()).isEmpty()) {
        holder.image.setImageResource(R.drawable.no_image);
      } else {
        Picasso.with(mContext).load(findUrlImage(items.getContent()))
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(holder.image);
      }

      holder.news.setOnClickListener(this);
      holder.news.setTag(position);
    }

  }

  @Override
  public Filter getFilter() {
    return exampleFilter;
  }

  private Filter exampleFilter = new Filter() {

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
      List<News> filteredList = new ArrayList<>();

      if (constraint == null || constraint.length() == 0) {
        filteredList.addAll(exampleListFull);
      } else {
        String filterPattern = constraint.toString().toLowerCase().trim();

        for (News item : exampleListFull) {
          if (item.getTitle().toLowerCase().contains(filterPattern.toString().toLowerCase())) {
            filteredList.add(item);
          }
        }
      }

      FilterResults results = new FilterResults();
      results.values = filteredList;
      return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
      mItems.clear();
      mItems.addAll((List) results.values);

      notifyDataSetChanged();
    }
  };

  @Override
  public int getItemCount() {
    return mItems.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.news:
        notifyDataSetChanged();
        eventGrammar.onClickDetail(position);
        break;

    }

  }

  /**
   * @author anhtt61
   * @version 1.0.1
   * @NewsViewHolder
   * @date 21/11/2021
   */
  public class GrammarViewHolder extends RecyclerView.ViewHolder {
    protected ImageView image;
    protected TextView title, date, author;
    protected RelativeLayout news;
    public GrammarViewHolder(View view) {
      super(view);
      image = view.findViewById(R.id.image);
      title = view.findViewById(R.id.title);
      date = view.findViewById(R.id.date);
      author = view.findViewById(R.id.author);
      news = view.findViewById(R.id.news);
    }
  }


  public String formatDate(String date) {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
    Date dateFormat = null;
    try {
      dateFormat = format.parse(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return format.format(dateFormat);
  }

  public String findUrlImage(String content) {
    if (content == null || content.isEmpty()) {
      return null;
    }
    String regex = "https?:/(?:/[^/]+)+\\.(?:jpg|gif|png|JPG)";
    Pattern pat = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
    Matcher matcher = pat.matcher(content);
    String url = null;
    while (matcher.find()) {
      if (content.substring(matcher.start(0),matcher.end(0)) != null ||
              content.substring(matcher.start(0),matcher.end(0)).length() > 0) {
        url = content.substring(matcher.start(0),matcher.end(0));
        break;
      }
    }
    return  url;
  }

  public interface EventGrammar {
    void onClickDetail(int position);
  }
}