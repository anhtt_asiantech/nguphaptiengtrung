package com.anhttvn.chinesegrammar.base.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.anhttvn.chinesegrammar.R;
import com.anhttvn.chinesegrammar.base.model.News;
import com.anhttvn.chinesegrammar.util.Config;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewAdapter extends PagerAdapter implements View.OnClickListener {

  private Context mContext;
  private List<News> news;
  private NewsEvent newsEvent;
  public NewAdapter(Context context, List<News> list, NewsEvent click) {
    this.mContext = context;
    this.news = list;
    this.newsEvent = click;
  }
  @Override
  public int getCount() {
    return news.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object o) {
    return view == o;
  }

  @Override
  public  Object instantiateItem(ViewGroup container, int position) {
      News itemNews = news.get(position);
      View view = LayoutInflater.from(mContext).inflate(R.layout.new_item_adapter, container, false);
      ImageView image = view.findViewById(R.id.imgNews);
      TextView title = view.findViewById(R.id.title);
      CardView cardNews = view.findViewById(R.id.cardNews);
      if (itemNews.getTitle() != null && !itemNews.getTitle().isEmpty()) {
        title.setText(itemNews.getTitle());
      }
      String imagePath = Config.findUrlImage(itemNews.getContent());
      if (imagePath == null || imagePath.isEmpty()) {
        image.setImageResource(R.drawable.no_image);
      } else {
        Picasso.with(mContext).load(imagePath)
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(image);
      }

      cardNews.setTag(position);
      cardNews.setOnClickListener(this);
      container.addView(view);

      return view;
  }


  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  @Override
  public int getItemPosition(Object object) {
    return super.getItemPosition(object);
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()) {
      case R.id.cardNews:
        newsEvent.detailNews(position);
        break;
      default:
        break;
    }
  }

  public interface NewsEvent {
    void detailNews(int position);
  }
}