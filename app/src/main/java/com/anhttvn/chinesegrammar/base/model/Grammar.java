package com.anhttvn.chinesegrammar.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Grammar implements Serializable {
  @SerializedName("items")
  @Expose
  public List<News> items;
  @SerializedName("nextPageToken")
  @Expose
  public String nextPageToken;
}