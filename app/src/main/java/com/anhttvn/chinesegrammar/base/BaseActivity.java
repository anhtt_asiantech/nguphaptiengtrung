package com.anhttvn.chinesegrammar.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.anhttvn.chinesegrammar.R;
import com.anhttvn.chinesegrammar.base.database.ControllerSQL;
import com.anhttvn.chinesegrammar.util.Connectivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public abstract class BaseActivity extends AppCompatActivity {

  protected Bundle savedInstanceState;
  protected ControllerSQL db;
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    savedInstanceState = savedInstanceState;
    setContentView(contentView());
    db = new ControllerSQL(this);
    configFullADS();
    init();
  }

  public abstract void init();
  public abstract View contentView();

  /**
   * show ads banner
   */
  public void isBannerADS (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      ads.loadAd(adRequest);
    }else{
      ads.setVisibility(View.GONE);
    }
  }

  private InterstitialAd mInterstitialAd;
  private AdRequest mAdRequest;

  protected void configFullADS() {
    MobileAds.initialize(getApplicationContext(),
            "ca-app-pub-3840180634112397~9187759690");

    mAdRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    mInterstitialAd = new InterstitialAd(this);
    mInterstitialAd.setAdUnitId(this.getString(R.string.banner_full_screen));
    if(isConnected()){
      mInterstitialAd.loadAd(mAdRequest);
    }

  }

  protected void isADSFull() {
    if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
      mInterstitialAd.show();
    }
  }

  protected boolean isConnected() {
    return Connectivity.isConnectedFast(this);
  }
}
