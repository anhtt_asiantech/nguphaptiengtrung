package com.anhttvn.chinesegrammar.base.api;


import com.anhttvn.chinesegrammar.base.model.Grammar;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("post?")
    Call<Grammar> getAllNews(@Query("key") String key);
    @GET("posts/search?")
    Call<Grammar> getNewOfLabel(@Query("q") String label, @Query("key") String key);

    @GET("posts?")
    Call<Grammar> getAllNewOfLabel(@Query("labels") String label, @Query("key") String key, @Query("pageToken") String nextPageToken);

//    @GET("label_tips_android.json")
//    Call<Label> getCategory();

}
