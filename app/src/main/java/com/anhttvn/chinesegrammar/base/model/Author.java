package com.anhttvn.chinesegrammar.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

@Data
public class Author implements Serializable {
  @SerializedName("displayName")
  @Expose
  public String displayName;
}